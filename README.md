# Intra-Detector Timing Study
___________________________________________
### Authors: Jordan Palmer, Yufan Qie (yqie2@u.rochester.edu)

The goal of this study is to correct for relative timing differences between channels LZ for each detector individually. There are a number of factors that can cause the relative timing between PMT channels to differ, such as

* The electron transit time for the PMTs within the TPC varies with the square root of the bias voltage. The result of PMT gain matching causes a transit time difference of ~10 ns
* Varying PMT cable lengths
* The pre- and post-amplifier’s shaping filters
* A 100 MHz clock pulse is propagated between each digitiser and can cause synchronisation delays between digitisers

This study consists of 3 steps:

1. ceate_subpods_allfiles: This step will select the LED-coincident photons for each channel. It will also chop these SPE pods such that we minimise the influence of the pre- and post-peak noise samples. These subpods can be used to generate templates or to produce the timing corrections. This step must be completed for every new dataset. Note, this code also accesses the trigger pulses. 

2. create_templates_allfiles: In this file we produce the average SPE shape for each channel (including the trigger channel). We calculate the average shape of peak-corrected SPEs per channel. We estimate the peak position to be (Amplitude * time)/len(Amplitude). We write the "scipy.interp1d" function to a file for each channel for future use. 

3. timing_studies: Finally, we fit the templates back to the channel SPEs and calcuate the peak times. For the trigger, we find the position of 90% of the peak. Based on the fitted peak times and trigger times, we obtain the timing distribution for each channel. The timing of each channel is represented by its 20% rising edge. We then apply a time of flight (TOF) correction to each channel. 
