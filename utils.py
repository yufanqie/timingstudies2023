import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.stats import norm
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
import math 
import random
import scipy.stats
import glob
import time 
import subprocess
from scipy.signal import find_peaks
import uproot 
#from ipywidgets import IntProgress
from IPython.display import display
import time
from tqdm import tqdm
import os
import utils 
import matplotlib.backends.backend_pdf
from hyper import hyper_dual_obj
import scipy
from scipy.signal import find_peaks
import numpy as np
from numpy import trapz
import matplotlib.pyplot as plt
import os
import mplhep
import matplotlib
from copy import deepcopy
import boost_histogram as bh
import itertools
from operator import itemgetter

def get_channel_IDs(detector,ladders=np.linspace(1,20,20),do_not_bias=[]):
    tpc_top_pmts = np.linspace(0,252,253,dtype=int)
    tpc_bottom_pmts = np.linspace(300,540,241,dtype=int)
    od_ladders = {'1':[819,839,859,879,899,919], '2': [818,838,858,878,898,918] , 
                  '3':[817,837,857,877,897,917], '4':[816,836,856,876,896,916], 
                  '5': [815,835,855,875,895,915],'6': [814,834,854,874,894,914],
                  '7':[813,833,853,873,893,913],'8':[812,832,852,872,892,912],
                  '9':[811,831,851,871,891,911],'10':[810,830,850,870,890,910],
                  '11':[809,829,849,869,889,909],'12':[808,828,848,868,888,908],
                  '13':[807,827,847,867,887,907],'14':[806,826,846,866,886,906],
                  '15':[805,825,845,865,885,905],'16':[804,824,844,864,884,904],
                  '17':[803,823,843,863,883,903],'18':[802,822,842,862,882,902],
                  '19':[801,821,841,861,881,901] ,'20':[800,820,840,860,880,900]}
    skin_bottom_pmts = np.linspace(600,692,(692-600)+1,dtype=int)
    skin_top_pmts    = np.linspace(700,719,(719-700)+1,dtype=int)
    skin_dome_pmts   = np.linspace(730,747,(747-730)+1,dtype=int)
    if detector=='TPC':
        tpc_pmts   = np.concatenate((tpc_bottom_pmts, tpc_top_pmts))
        tpc_pmts = [bad for bad in tpc_pmts if bad not in do_not_bias]
        return tpc_pmts
    elif detector=='TTPC':
        tpc_pmts   = tpc_top_pmts
        tpc_pmts = [bad for bad in tpc_pmts if bad not in do_not_bias]
        return tpc_pmts
    elif detector=='BTPC':
        tpc_pmts   = tpc_bottom_pmts
        tpc_pmts = [bad for bad in tpc_pmts if bad not in do_not_bias]
        return tpc_pmts
    elif detector=='OD':
        ladders_chosen=[str(int(x)) for x in ladders]
        pmtidsladders = [itemgetter(x)(od_ladders) for x in ladders_chosen]
        od_pmts = [item for sublist in pmtidsladders for item in sublist]
        od_pmts = [bad for bad in od_pmts if bad not in do_not_bias]
        return od_pmts
    elif detector=='Skin':
        skin_pmts = np.concatenate((skin_bottom_pmts, skin_top_pmts))
        skin_pmts = [bad for bad in skin_pmts if bad not in do_not_bias]
        return skin_pmts
    elif detector=='SkinBarrel':
        skin_pmts    = np.concatenate((skin_bottom_pmts, skin_top_pmts))
        skin_pmts = [bad for bad in skin_pmts if bad not in do_not_bias]
        return skin_pmts
    elif detector=='SkinDome':
        dome_pmts = [bad for bad in skin_dome_pmts if bad not in do_not_bias]
        return dome_pmts
    elif detector=='TSkin':
        skin_pmts = np.linspace(600,692,(692-600)+1,dtype=int)
        skin_pmts = [bad for bad in skin_pmts if bad not in do_not_bias]
        return skin_pmts
    elif detector=='BSkin':
        skin_pmts = np.linspace(700,719,(719-700)+1,dtype=int)
        skin_pmts = [bad for bad in skin_pmts if bad not in do_not_bias]
        return skin_pmts
    
    
def getEventPODs(PODindex, eventNumber):
    """ get the POD indices corresponding to the event number (eventNumber)
        args: 
        PODindex: using raw RQ : ['Event']['firstData'] , indices of PODs
        eventNumber: event you want to look at 
        returns: min and max range of PODs in that event
    """ 
    if eventNumber >= len(PODindex):
        print("there are not this many events in this file, please enter a number less than " + str(len(PODindex)))
        return -1
    else:
        PODmin = PODindex[eventNumber - 1] #event 1 is 0th element
        PODmax = PODindex[eventNumber]
        return int(PODmin), int(PODmax)
    
    
def findPODHeight(POD):
    """ Function to calculate the amplitudes of the peak(s) within a POD 

    Note: The scipy.signal.find_peaks might not find the peak in the input POD since the prominence is defined
    as the height above the lowest point to the left of the peak. This "lowest point" will be different when considering
    the entire waveform than the POD alone. We have found this to give us 0.5% disagreement so be sure to track this 
    where possible. A solution to this might be to store the heights the first time around. 
    """
    ###find the peaks again
    peaks,_ = scipy.signal.find_peaks(POD, height=np.mean(POD[0:30])+20) #prominence=28,height = np.mean(POD[0:30])+10    -8000)#np.mean(POD[0:20]))   #28 prom
    amplitudes=[]
    for i in  _['peak_heights']:
        amplitudes.append(i - np.mean(POD[0:30])) #subtract baseline average from height
    return amplitudes,peaks

def findPODArea(POD):
    from numpy import trapz
    """ Function to calculate the areas of the PODs

    Note: The scipy.signal.find_peaks might not find the peak in the input POD since the prominence is defined
    as the height above the lowest point to the left of the peak. This "lowest point" will be different when considering
    the entire waveform than the POD alone. We have found this to give us 0.5% disagreement so be sure to track this 
    where possible. A solution to this might be to store the heights the first time around. 
    """

    ###find the peaks again
    halfwidth = 20
    #print(scipy.signal.find_peaks(POD, np.mean(POD[0:30])+10))
    #try:
    peaks,_ = scipy.signal.find_peaks(POD, height=np.mean(POD[0:30])+20) #prominence=28,height = -8000)#np.mean(POD[0:20]))   #28 prom #np.mean(POD[0:30])+20
    if len(peaks)==0:
        return -1,-1
    else:
        lowlimit = peaks[0] - halfwidth
        baseline_average = np.mean(POD[0:30])
        if len(peaks)==1: #only one peak 
            highlimit = peaks[0] + halfwidth
        else: 
            return -1,-1
        area = np.trapz(POD[lowlimit:highlimit]-baseline_average, dx=1) #subtract baseline average too 
        return area,peaks
        
        
def get_fit_window(bin_counts, bin_edges, low_edge=0.6, upper_edge=0.2):###Austin's technique for defining fit bounds
    """
    Determine the range of pulse areas to be used in Gaussian fit for
    extracting the PMT gain.
    params:
        bin_counts - number of entries in each bin
        bin_edges - location of each bin edge; contains (n_bins+1) entries
    returns:
        index_min - lower index for fitting range
        index_max - upper index for fitting range
        peak - index corresponding to peak area
    """
    # Find peak area and corresponding index, along with start of distribution
    peak = np.argwhere(bin_counts == max(bin_counts[5:]))[0][-1]
   # print(peak)
    peak_area = bin_edges[peak]
    lower_thresh_index = np.argwhere(bin_edges > 4)[0][0]
    # Left edge: tighter restriction to avoid including noise shoulder
    left_fit_indices = np.argwhere((bin_counts < low_edge * max(bin_counts[lower_thresh_index:])))[:, 0]
    try:
        index_min = left_fit_indices[np.argwhere(left_fit_indices < peak)[-1]]
    except IndexError as error:
        print("Error in get_fit_window: failed to obtain lower index for fitting window.")
        return None, None, None
    # Right edge: range is limited to include DPE+ pulses (1/7 is typically good)
    right_fit_indices = np.argwhere((bin_counts < (upper_edge) * max(bin_counts[lower_thresh_index:])))[:,0]
    try:
        index_max = right_fit_indices[np.argwhere(right_fit_indices > peak)[0]]
    except:
        index_max = len(bin_edges)
    
    return int(index_min), int(index_max), peak

def create_subpods(inputs):
    df_channel = inputs[0]
    channel = inputs[1]
    event_number = df_channel['eventnumber']
    final_event_numbers = []
    if channel < 2100:
        peak_find_result = list(map(findPODArea, -1*df_channel['pods']))
        area = [peak_find_result[i][0] for i in range(len(peak_find_result))]
        peaks = [peak_find_result[i][1] for i in range(len(peak_find_result))]
        realistic_area = [area[i] for i in range(len(area)) if area[i] < 1000]
        realistic_peak = [peaks[i] for i in range(len(peaks)) if area[i] < 1000]
        mean_area = np.mean(realistic_area)
        stdev_area = np.std(realistic_area)
        min_area = mean_area - stdev_area
        max_area = mean_area + stdev_area
        if min_area <= 30:
            print('This channel has an area min equal to threshold + 5 samples, this is a bad channel')
    interp_pod,peaks_all,pod_time,sub_pods = [],[],[],[]
    peak_estimations,peak_full = [],[]

       # return -1,-1
    ### loop over individual pods for this channel
    pod_exists = False
    for i in range(len(df_channel['pods'])):
        ### select only single-peak pods
        if channel < 2100:
            if (area[i] == -1) or (area[i] < min_area) or (area[i] > max_area):
                continue
            else: ###trigger sometimes gives -1
                if peaks[i] == -1:
                    continue
        ### select pods close to trigger time
        ### this will likely change for each detector system
        if channel < 2100:
            if (df_channel['startTimesRelTrigger'].values[i] < 50) or (df_channel['startTimesRelTrigger'].values[i] > 100):
                continue
        # else: ### this is the LED trigger pulse
        #     if (df_channel['startTimesRelTrigger'].values[i] < -50) or (df_channel['startTimesRelTrigger'].values[i] > 0):
        #         continue
        ### determine the sub-pod
        ## might need to check this doesnt extend past the pod limits
        baselinesub_pod = -1*df_channel['pods'][i] - np.mean(-1*df_channel['pods'][i][0:20])
        pod_exists = True
        if channel < 2100:
            sub_pod_peak_time = 10
            sub_pod = baselinesub_pod[peaks[i][0] - sub_pod_peak_time:peaks[i][0] + 15]
            pod_subpod_diff   = peaks[i][0] - sub_pod_peak_time ## original peak position - new peak position = number of start samples cut off
            sub_pod_rel_event = df_channel['startTimesRelTrigger'].values[i] + pod_subpod_diff
        else: ###trigger pulse
            sub_pod_peak_time = 20
            ### define the NIM time as 90% of maximum
            time_at_this = 0.9*np.max(baselinesub_pod)
            max_point = np.argwhere(baselinesub_pod == np.max(baselinesub_pod))[0][0]
            loc_time = [np.argwhere(baselinesub_pod <= time_at_this)[x][0] for x in range(len(np.argwhere(baselinesub_pod <= time_at_this)))]
            point_90p = [loc_time[x] for x in range(len(loc_time)-1) if loc_time[x+1]- loc_time[x] !=1][0]
            sub_pod = baselinesub_pod[point_90p - sub_pod_peak_time:point_90p + 30]
            pod_subpod_diff   = point_90p - sub_pod_peak_time ## original peak position - new peak position = number of start samples cut off
            sub_pod_rel_event = df_channel['startTimesRelTrigger'].values[i] + pod_subpod_diff
        final_event_numbers.append(event_number[i])
        sub_pods.append(sub_pod)
        pod_time.append(sub_pod_rel_event)
    if pod_exists == True:
        return sub_pods,pod_time,final_event_numbers
    else:
        return -1,-1,-1
        
        
def create_average_shape(inputs,is_trigger=False):
    df = inputs[0]
    # = inputs[1]
    ### loop over individual pods for this channel
    interp_pod=[]
    if is_trigger == True:
        loop_lim = df['subpods']
    else:
        loop_lim = df['subpods']
    for i in range(len(loop_lim)):
        if is_trigger==True:
            time_at_this = 0.9*np.max(df['subpods'][i])
            point_90p = np.argmax(df['subpods'][i]>time_at_this)
            peak_estimation = point_90p
            x=np.arange(0, len(df['subpods'][i]))
            interpfunc=scipy.interpolate.interp1d(x,df['subpods'][i],'linear',fill_value=0,bounds_error=False)
            new_temp_x = np.arange(-50,100,0.01)
            #### correct by peak position
            interp_fine = interpfunc(new_temp_x - peak_estimation)/max(interpfunc(new_temp_x -peak_estimation))
            interp_pod.append(interp_fine)           
        else:
            peak_estimation = sum(df['subpods'][i][5:15] * np.arange(0,len(df['subpods'][i][5:15])))/sum(df['subpods'][i][5:15])
            x=np.arange(0, len(df['subpods'][i]))
            new_temp_x = np.arange(0,100,0.1)
            interpfunc=scipy.interpolate.interp1d(x,df['subpods'][i],'linear',fill_value=0,bounds_error=False)
            #### correct by peak position
            interp_fine = interpfunc(new_temp_x - peak_estimation)/max(interpfunc(new_temp_x -peak_estimation))
            #plt.plot(new_temp_x,interp_fine)
            interp_pod.append(interp_fine[0:400])
    mean=np.sum(interp_pod,axis=0)/len(interp_pod)
    x_new = np.arange(0,int(len(mean)/10),.1)### puts the template in the correct domain, roughly
    interpfunc_final=scipy.interpolate.interp1d(x_new,mean,'linear',fill_value=0,bounds_error=False)
    return interpfunc_final#,pod_time,sub_pods

def dir_finder(path_prefix,run_ID):
    com_data_path = path_prefix
    direc_final = ''
    subdirec_final = ''
    for direc in os.listdir(com_data_path):
            for subdirec in os.listdir(com_data_path+direc):
                if subdirec.endswith(str(run_ID)):
                    direc_final = direc+'/'
                    subdirec_final = subdirec
                    break 
    directory = com_data_path + direc_final + subdirec_final
#     print(directory)
    files = []
    for file in os.listdir(directory):
        if file.endswith(".root"):
            files += [os.path.join(directory, file)]
    if (len(files) == 0):
        raise ValueError("No files found. Please check data path")
    files = sorted(files)
    for i in range(len(files)):
        files[i] = files[i].split("\\")[-1]
#     print(files)
    return files
def get_file_paths(path_prefix, runs):
    files=[]
    for i in runs:
        files_ =dir_finder(path_prefix,i)## find files using dir finder. Catalog can be wrong sometimes
        files.append(files_)

    flat_list = [item for sublist in files for item in sublist]
    return flat_list